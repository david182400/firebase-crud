import 'package:models/custom_exception.dart';
import 'package:models/user.dart';
import 'package:models/web_service_response.dart';


mixin UserRepository {
  Future<WebServiceResponse<List<User>>> getUsers();

  Future<WebServiceResponse<bool>> addUser(User user);

  Future<WebServiceResponse<bool>> editUser(User user);

  Future<WebServiceResponse<bool>> deleteUser(String id);

  Stream<List<User>> streamUsers();

  Future<WebServiceResponse> getPlacesHttp(String typedValue);
}

mixin UserUseCase {
  Future<WebServiceResponse<List<User>>> getUsers();

  Future<WebServiceResponse<bool>> addEditUser(User user, bool isAdd);

  Future<WebServiceResponse<bool>> deleteUser(String id);

  Stream<List<User>> streamUsers();

  Future<WebServiceResponse> getPlacesHttp(String typedValue);
}


class UserUseCaseImpl implements UserUseCase {

  final UserRepository _userRepository;

  UserUseCaseImpl(this._userRepository);

  @override
  Future<WebServiceResponse<List<User>>> getUsers() {
    return _userRepository.getUsers();
  }

  @override
  Future<WebServiceResponse<bool>> addEditUser(User user, bool isAdd) async {
    if (validateFields(user)) {
      if (isAdd) {
        return await _userRepository.addUser(user);
      } else {
        return await _userRepository.editUser(user);
      }
    } else {
      throw CustomException('Data unavailable');
    }
  }

  bool validateFields(User user) {
    final fields = [user.name, user.age, user.address, user.phone];
    for (int i = 0; i < fields.length; i++) {
      if (fields[i] == null || fields[i] == '') {
        return false;
      }
    }
    return true;
  }

  @override
  Future<WebServiceResponse<bool>> deleteUser(String id) {
    return _userRepository.deleteUser(id);
  }

  @override
  Stream<List<User>> streamUsers() {
    return _userRepository.streamUsers();
  }

  @override
  Future<WebServiceResponse> getPlacesHttp(String typedValue) {
    return _userRepository.getPlacesHttp(typedValue);
  }

}