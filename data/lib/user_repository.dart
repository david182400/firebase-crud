import 'package:domain/user_user_case.dart';
import 'package:models/user.dart';
import 'package:models/web_service_response.dart';


mixin UsersFirestoreSource {
  Future<WebServiceResponse<List<User>>> getUsers();

  Future<WebServiceResponse<bool>> addUser(User user);

  Future<WebServiceResponse<bool>> editUser(User user);

  Future<WebServiceResponse<bool>> deleteUser(String id);

  Stream<List<User>> streamUsers();
}

mixin PlacesApiSource {
  Future<WebServiceResponse> getPlaces(String typedValue);
}


class UserRepositoryImpl implements UserRepository {

  final UsersFirestoreSource _apiSource;
  final PlacesApiSource _placesApiSource;
  UserRepositoryImpl(this._apiSource, this._placesApiSource);

  @override
  Future<WebServiceResponse<List<User>>> getUsers() {
    return _apiSource.getUsers();
  }

  @override
  Future<WebServiceResponse<bool>> addUser(User user) {
    return _apiSource.addUser(user);
  }

  @override
  Future<WebServiceResponse<bool>> editUser(User user) {
    return _apiSource.editUser(user);
  }

  @override
  Future<WebServiceResponse<bool>> deleteUser(String id) {
    return _apiSource.deleteUser(id);
  }

  @override
  Stream<List<User>> streamUsers() {
    return _apiSource.streamUsers();
  }

  @override
  Future<WebServiceResponse> getPlacesHttp(String typedValue) {
    return _placesApiSource.getPlaces(typedValue);
  }

}