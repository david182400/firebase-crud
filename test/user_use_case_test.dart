import 'package:domain/user_user_case.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:models/user.dart';
import 'package:models/web_service_response.dart';

import 'user_use_case_test.mocks.dart';


mixin UserRepositoryTest implements UserRepository {
  @override
  Future<WebServiceResponse<bool>> addUser(User user);

  @override
  Future<WebServiceResponse<bool>> deleteUser(String id);

  @override
  Future<WebServiceResponse<bool>> editUser(User user);

  @override
  Future<WebServiceResponse<List<User>>> getUsers();

  @override
  Stream<List<User>> streamUsers();
}

@GenerateMocks([UserRepositoryTest])
void main() {
  UserUseCaseImpl userUseCaseImpl;
  MockUserRepositoryTest mockRepository;
  User user;

  setUp((){
    mockRepository = MockUserRepositoryTest();
    userUseCaseImpl = UserUseCaseImpl(mockRepository);
    user = User();
    user.id = '1';
    user.name = '1';
    user.age = 0;
    user.address = '1';
    user.phone = 0;
  });

  test('getUsers return a WebService object of type list of users', () async {
    var expected = WebServiceResponse<List<User>>(result: [User(), User()]);
    when(mockRepository.getUsers()).thenAnswer((_) async => expected);
    var result = await userUseCaseImpl.getUsers();

    expect(result, expected);
    verify(mockRepository.getUsers());
  });

  test('addEditUser return true when adding successfully an user', () async {
    var expected = WebServiceResponse<bool>(result: true);
    when(mockRepository.addUser(user)).thenAnswer((_) async => expected);
    var result = await userUseCaseImpl.addEditUser(user, true);

    expect(result, expected);
    verify(mockRepository.addUser(user));
  });

  test('addEditUser return true when editing successfully an user', () async {
    var expected = WebServiceResponse<bool>(result: true);
    when(mockRepository.editUser(user)).thenAnswer((_) async => expected);
    var result = await userUseCaseImpl.addEditUser(user, false);

    expect(result, expected);
    verify(mockRepository.editUser(user));
  });

  test('deleteUser return true when deleting successfully an user', () async {
    var expected = WebServiceResponse<bool>(result: true);
    when(mockRepository.deleteUser('')).thenAnswer((realInvocation) async => expected);
    var result = await userUseCaseImpl.deleteUser('');

    expect(result, expected);
    verify(mockRepository.deleteUser(''));
  });

  test('getUsers return a Stream list of users', () async {
    var expected = Stream.value([User(), User()]);
    when(mockRepository.streamUsers()).thenAnswer((realInvocation) => expected);
    var result = userUseCaseImpl.streamUsers();

    expect(result, expected);
  });

  test('validate fields return true when an user argument type has all fields with data', (){
    bool expected = true;

    var result = userUseCaseImpl.validateFields(user);

    expect(result, expected);
  });

  test('validate fields return fale when an user argument type has at least one empty field', (){
    bool expected = false;

    var result = userUseCaseImpl.validateFields(User());

    expect(result, expected);
  });

}