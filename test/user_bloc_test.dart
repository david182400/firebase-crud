import 'dart:async';

import 'package:domain/user_user_case.dart';
import 'package:firebase_crud/bloc/user_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:models/user.dart';
import 'package:models/web_service_response.dart';

import 'user_bloc_test.mocks.dart';


mixin UserUseCaseTest implements UserUseCase {
  @override
  Future<WebServiceResponse<bool>> addEditUser(User user, bool isAdd);
  
  @override
  Future<WebServiceResponse<bool>> deleteUser(String id);

  @override
  Future<WebServiceResponse<List<User>>> getUsers();

  @override
  Stream<List<User>> streamUsers();
}

@GenerateMocks([UserUseCaseTest])
void main() {
  UserBloc userBloc;
  MockUserUseCaseTest mockUseCase;
  User user;

  setUp(() {
    mockUseCase = MockUserUseCaseTest();
    userBloc = UserBloc(mockUseCase);
    user = User();
    user.id = '';
    user.name = '';
    user.age = 0;
    user.address = '';
    user.phone = 0;
  });

  test('getUsers return a WebService object of type list of users', () async {
    var expected = WebServiceResponse<List<User>>(result: [User(), User()]);
    when(mockUseCase.getUsers()).thenAnswer((_) async => expected);
    var result = await userBloc.getUsers();

    expect(result, expected);
    expect(result, userBloc.listMovies.valueWrapper.value);
  });

  test('addEditUser return true when adding successfully an user', () async {
    var expected = WebServiceResponse<bool>(result: true);

    when(mockUseCase.addEditUser(user, true)).thenAnswer((_) async => expected);
    var result = await userBloc.addEditUser(user, true);

    expect(result, expected);
  });

  test('addEditUser return true when editing successfully an user', () async {
    var expected = WebServiceResponse<bool>(result: true);
    when(mockUseCase.addEditUser(user, false)).thenAnswer((_) async => expected);
    var result = await userBloc.addEditUser(user, false);

    expect(result, expected);
  });

  test('deleteUser return true when deleting successfully an user', () async {
    var expected = WebServiceResponse<bool>(result: true);
    when(mockUseCase.deleteUser('')).thenAnswer((realInvocation) async => expected);
    var result = await userBloc.deleteUser('');

    expect(result, expected);
  });

  test('getUsers return a Stream list of users', () async {
    var expected = Stream.value([User(), User()]);
    var expectedStream = WebServiceResponse<List<User>>(result: [User(), User()]);

    StreamController<WebServiceResponse<List<User>>> controller = StreamController();
    var stream = controller.stream;

    when(mockUseCase.streamUsers()).thenAnswer((realInvocation) => expected);
    var result = userBloc.streamUsers();

    expect(result, expected);

    stream.listen((event) {
      expect(event, expectedStream);
      controller.close();
    });

    controller.add(expectedStream);

    // userBloc.listMovies.listen((value) {
    //   expectAsync1<void, WebServiceResponse<List<User>>>((event) {
    //       expect(event, expectedStream);
    //     },
    //   );
    // });

  });

}