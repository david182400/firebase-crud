

import 'package:data/user_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:models/user.dart';
import 'package:models/web_service_response.dart';

import 'user_repository_test.mocks.dart';

mixin FirestoreSourceTest implements UsersFirestoreSource {
  @override
  Future<WebServiceResponse<bool>> addUser(User user);

  @override
  Future<WebServiceResponse<bool>> deleteUser(String id);

  @override
  Future<WebServiceResponse<bool>> editUser(User user);

  @override
  Future<WebServiceResponse<List<User>>> getUsers();

  @override
  Stream<List<User>> streamUsers();
}

@GenerateMocks([FirestoreSourceTest])
void main() {
  UserRepositoryImpl userRepositoryImpl;
  MockFirestoreSourceTest mockFireStore;
  User user;

  setUp((){
    mockFireStore = MockFirestoreSourceTest();
    userRepositoryImpl = UserRepositoryImpl(mockFireStore, null);
    user = User();
    user.id = '1';
    user.name = '1';
    user.age = 0;
    user.address = '1';
    user.phone = 0;
  });

  test('getUsers return a WebService object of type list of users', () async {
    var expected = WebServiceResponse<List<User>>(result: [User(), User()]);
    when(mockFireStore.getUsers()).thenAnswer((_) async => expected);
    var result = await userRepositoryImpl.getUsers();

    expect(result, expected);
  });

  test('addEditUser return true when adding successfully an user', () async {
    var expected = WebServiceResponse<bool>(result: true);
    when(mockFireStore.addUser(user)).thenAnswer((_) async => expected);
    var result = await userRepositoryImpl.addUser(user);

    expect(result, expected);
  });

  test('addEditUser return true when editing successfully an user', () async {
    var expected = WebServiceResponse<bool>(result: true);
    when(mockFireStore.editUser(user)).thenAnswer((_) async => expected);
    var result = await userRepositoryImpl.editUser(user);

    expect(result, expected);
  });

  test('deleteUser return true when deleting successfully an user', () async {
    var expected = WebServiceResponse<bool>(result: true);
    when(mockFireStore.deleteUser('')).thenAnswer((realInvocation) async => expected);
    var result = await userRepositoryImpl.deleteUser('');

    expect(result, expected);
  });

  test('getUsers return a Stream list of users', () async {
    var expected = Stream.value([User(), User()]);
    when(mockFireStore.streamUsers()).thenAnswer((realInvocation) => expected);
    var result = userRepositoryImpl.streamUsers();

    expect(result, expected);
  });

}