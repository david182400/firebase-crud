class Messages {

  static const String internetUnavailable = 'No internet connection';
  static const String dataIsEmpty = 'Data is empty';
  static const String fireStoreError = 'FireStore error';
  static const String errorUnpackingData = 'Error unpacking data';

}