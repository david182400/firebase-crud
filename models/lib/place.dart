class Place {

  String id;
  String title;
  String address;
  double lat;
  double lng;

  Place();

  Place.fromJson(Map<String, dynamic> jsonMap) {
    id = jsonMap['id'];
    title = jsonMap['title'];
    var add = jsonMap['address'];
    address = add['label'];
    var pos = jsonMap['position'];
    lat = pos['lat'];
    lng = pos['lng'];
  }

}