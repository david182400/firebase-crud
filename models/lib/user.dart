class User {

  String id;
  String name;
  int age;
  String address;
  int phone;

  User();

  User.fromJson(Map<String, dynamic> jsonMap) {
    id =  jsonMap['id'];
    name = jsonMap['name'];
    age = jsonMap['age'] is String ? int.parse(jsonMap['age']) : jsonMap['age'];
    address = jsonMap['address'];
    phone = jsonMap['phone'] is String ? int.parse(jsonMap['phone']) : jsonMap['phone'];
  }

  Map<String, dynamic> toMap() {
    return {
      'name' : name,
      'age' : age,
      'address' : address,
      'phone' : phone
    };
  }

}