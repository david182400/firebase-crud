class WebServiceResponse<T> {

  T result;
  String errorMsg;

  WebServiceResponse({
    this.result,
    this.errorMsg = '',
  });
}