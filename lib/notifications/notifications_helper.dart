import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:rxdart/rxdart.dart';


class NotificationsHelper {

  BehaviorSubject<Map<String, dynamic>> _behaviorSubject = BehaviorSubject<Map<String, dynamic>>();
  Stream<Map<String, dynamic>> get streamNotification => _behaviorSubject.stream;
  set setData(Map<String, dynamic> data) {
    _behaviorSubject.add(data);
  }

  AndroidNotificationChannel channel = const AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    'This channel is used for important notifications.', // description
    importance: Importance.high,
  );

  static NotificationsHelper _instance;
  factory NotificationsHelper() {
    if (_instance == null) {
      _instance = NotificationsHelper._();
    }
    return _instance;
  }
  NotificationsHelper._();

  Future _onMessage(RemoteMessage message) async {
    print('_onMessage: ${message.data['name']}');
    RemoteNotification notification = message.notification;
    AndroidNotification android = message.notification?.android;
    if (notification != null && android != null) {
      FlutterLocalNotificationsPlugin().initialize(
        InitializationSettings(android: AndroidInitializationSettings('icon_launch')),
        onSelectNotification: (payload) {
          _behaviorSubject.add(message.data);
          return Future.value(null);
        },
      );
      FlutterLocalNotificationsPlugin().show(
          notification.hashCode, notification.title, notification.body,
          NotificationDetails(
            android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              channel.description,
              icon: 'icon_launch'
            ),
          ));
    }
  }

  Future _onMessageOpenedApp(RemoteMessage message) async {
    print('_onMessageOpenedApp: ${message.data['name']}');
    _behaviorSubject.add(message.data);
  }

  initializedMethods() {
    FirebaseMessaging.onBackgroundMessage(_onBackgroundMessage);
    FirebaseMessaging.onMessage.listen(_onMessage);
    FirebaseMessaging.onMessageOpenedApp.listen(_onMessageOpenedApp);
  }

  void dispose() {
    _behaviorSubject.close();
  }

}

Future _onBackgroundMessage(RemoteMessage message) async {
  print('_onBackgroundMessage: ${message.data['name']}');
  NotificationsHelper().setData = message.data;
}