import 'package:flutter/material.dart';


class ShowErrorScreen extends StatelessWidget {
  final String errorMsg;
  final Future<void> Function() onRefresh;
  const ShowErrorScreen({@required this.errorMsg, @required this.onRefresh});

  @override
  Widget build(BuildContext context) {
    return Container(
        child: RefreshIndicator(
          child: Stack(
            children: <Widget>[
              Center(child: Text(errorMsg),),
              ListView()
            ],
          ),
          onRefresh: onRefresh,
        )
    );
  }
}