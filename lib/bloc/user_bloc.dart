import 'dart:async';

import 'package:domain/user_user_case.dart';
import 'package:models/custom_exception.dart';
import 'package:models/user.dart';
import 'package:models/web_service_response.dart';
import 'package:rxdart/rxdart.dart';


class UserBloc {

  final UserUseCase _userUseCase;

  final _listUsersSubject = BehaviorSubject<WebServiceResponse<List<User>>>();
  ValueStream<WebServiceResponse<List<User>>> get listMovies => _listUsersSubject.stream;

  StreamSubscription<List<User>> _usersSubs;

  UserBloc(this._userUseCase);


  Future<WebServiceResponse<List<User>>> getUsers() {
    return _userUseCase.getUsers().then((value) {
      _listUsersSubject.add(value);
      return value;
    }).catchError((error) {
      if (error is CustomException) {
        _listUsersSubject.add(WebServiceResponse(errorMsg: error.msg));
      } else {
        _listUsersSubject.add(WebServiceResponse(errorMsg: error));
      }
    });
  }

  Future<WebServiceResponse<bool>> addEditUser(User user, bool isAdd) {
    return _userUseCase.addEditUser(user, isAdd);
  }

  Future<WebServiceResponse<bool>> deleteUser(String id) {
    return _userUseCase.deleteUser(id);
  }

  void dispose() {
    _listUsersSubject?.close();
    _usersSubs?.cancel();
  }

  Stream<List<User>> streamUsers() {
    var result = _userUseCase.streamUsers();
    _usersSubs = result.listen((values) {
      _listUsersSubject.add(WebServiceResponse(result: values));
    });
    return result;
  }

}