import 'package:domain/user_user_case.dart';
import 'package:google_place/google_place.dart';
import 'package:models/custom_exception.dart';
import 'package:models/web_service_response.dart';
import 'package:rxdart/rxdart.dart';


class SearchPlacesBloc {

  final UserUseCase _userUseCase;

  SearchPlacesBloc(this._userUseCase);

  final _listPlacesSubject = BehaviorSubject<WebServiceResponse>();
  ValueStream<WebServiceResponse> get listPlaces => _listPlacesSubject.stream;

  Future<List<AutocompletePrediction>> getPlaces(String typedValue) async {
    String apiKey = 'API_KEY_HERE';
    GooglePlace googlePlace = GooglePlace(apiKey);

    var result = await googlePlace.autocomplete.get(typedValue);
    if (result != null && result.predictions != null ) {
      //predictions = result.predictions;
    }
  }

  Future<WebServiceResponse> getPlacesHttp(String typedValue) async {
    return _userUseCase.getPlacesHttp(typedValue).then((value) {
      _listPlacesSubject.add(value);
      return value;
    }).catchError((onError) {
      if (onError is CustomException) {
        var errorMsg = onError.msg;
        _listPlacesSubject.add(WebServiceResponse(errorMsg: errorMsg));
      } else {
        _listPlacesSubject.add(WebServiceResponse(errorMsg: onError));
      }
    });
  }

  void dispose() {
    _listPlacesSubject?.close();
  }

}