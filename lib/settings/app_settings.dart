class AppSettings {

  static AppSettings _instance;

  String collectionName;

  String notificationToken;

  String baseUrl;

  factory AppSettings() {
    if (_instance == null) {
      _instance = AppSettings._();
    }
    return _instance;
  }

  AppSettings._();

}