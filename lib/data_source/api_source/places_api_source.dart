import 'package:data/user_repository.dart';
import 'package:firebase_crud/data_source/api_source/api_base.dart';
import 'package:firebase_crud/utils/connectivity/my_connectivity.dart';
import 'package:models/custom_exception.dart';
import 'package:models/messages.dart';
import 'package:models/place.dart';
import 'package:models/web_service_response.dart';


class PlacesApiSourceAdapter extends ApiBase implements PlacesApiSource{

  PlacesApiSourceAdapter(String baseUrl, MyConnectivity connectivity)
      : super(baseUrl, connectivity);

  @override
  Future<WebServiceResponse> getPlaces(String typedValue) async {
    List<Place> _listPlaces = [];
    return getData(30, baseUrl, typedValue).then((response) {
      if (response.result != null && !response.result.isEmpty) {
        for (var data in response.result['items']) {
          _listPlaces.add(Place.fromJson(data));
        }
        return WebServiceResponse(result: _listPlaces);
      } else {
        throw CustomException(Messages.errorUnpackingData);
      }
    });
  }



}