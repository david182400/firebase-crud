import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_crud/utils/connectivity/my_connectivity.dart';
import 'package:models/custom_exception.dart';
import 'package:models/messages.dart';
import 'package:models/web_service_response.dart';
import 'package:http/http.dart' as http;


class ApiBase {

  final String baseUrl;
  final MyConnectivity connectivity;

  ApiBase(this.baseUrl, this.connectivity);

  Future<WebServiceResponse> getData(int timeout, String url, String typedValue) async {
    try {
      var connectivityResult = await connectivity.checkConnectivity();
      if (connectivityResult == ConnectivityResult.none) {
        throw(Messages.internetUnavailable);
      }
      String queryUri = Uri(queryParameters: {
        'apiKey' : '0gxtPrArZLFBVoQ7cF7RbP9Q07gDCbXxXSIl27yCayM',
        'q' : typedValue,
        'at' : '7.120568,-73.121878'
      }).query;
      var requestUrl = url + '?' + queryUri;
      var response = await http.get(Uri.parse(requestUrl),).timeout(Duration(seconds: timeout));
      var json = jsonDecode(response.body);
      if (response.statusCode == 200) {
        return WebServiceResponse(result: json);
      } else {
        throw('Status code: ${response.statusCode}');
      }
    } catch (e) {
      return throw new CustomException('$e');
    }
  }

}