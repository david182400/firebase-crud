import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_crud/utils/connectivity/my_connectivity.dart';
import 'package:models/custom_exception.dart';
import 'package:models/messages.dart';
import 'package:models/web_service_response.dart';



class FireStoreBase {

  final String collectionName;
  final MyConnectivity connectivity;

  FireStoreBase(this.collectionName, this.connectivity);

  Future<WebServiceResponse<QuerySnapshot>> getData() async {
    CollectionReference users = FirebaseFirestore.instance.collection(collectionName);
    try {
      var connectivityResult = await connectivity.checkConnectivity();
      if (connectivityResult == ConnectivityResult.none) {
        throw(Messages.internetUnavailable);
      }
      return users.get().then((QuerySnapshot querySnapshot) {
        if (querySnapshot.docs.isEmpty) {
          throw (Messages.dataIsEmpty);
        } else {
          return WebServiceResponse<QuerySnapshot>(result: querySnapshot);
        }
      }).catchError((error) {
        throw (Messages.fireStoreError + ' $error');
      });
    } catch (e) {
      print('error ${e.toString()}');
      throw CustomException('$e');
    }
  }

  Future<WebServiceResponse<bool>> addEditData(Map<String, dynamic> data, bool isAdd, {String dataId}) async {
    CollectionReference users = FirebaseFirestore.instance.collection(collectionName);
    try {
      if (isAdd) {
        return users.add(data).then((value) {
          return WebServiceResponse<bool>(result: true);
        }).catchError((error) {
          throw (Messages.fireStoreError + ' $error');
        });
      } else {
        return users.doc(dataId).update(data).then((value) {
          return WebServiceResponse<bool>(result: true);
        }).catchError((error) {
          throw (Messages.fireStoreError + ' $error');
        });
      }
    } catch (e) {
      print('error ${e.toString()}');
      throw CustomException('$e');
    }
  }

  Future<WebServiceResponse<bool>> deleteData(String dataId) {
    CollectionReference users = FirebaseFirestore.instance.collection(collectionName);
    try {
      return users.doc(dataId).delete().then((value) {
        return WebServiceResponse<bool>(result: true);
      }).catchError((error) {
        throw (Messages.fireStoreError + ' $error');
      });
    } catch (e) {
      print('error ${e.toString()}');
      throw CustomException('$e');
    }
  }

}