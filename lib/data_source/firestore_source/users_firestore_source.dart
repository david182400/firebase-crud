import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data/user_repository.dart';
import 'package:firebase_crud/data_source/firestore_source/firestore_base.dart';
import 'package:firebase_crud/utils/connectivity/my_connectivity.dart';
import 'package:models/custom_exception.dart';
import 'package:models/messages.dart';
import 'package:models/user.dart';
import 'package:models/web_service_response.dart';


class FireStoreSourceImpl extends FireStoreBase implements UsersFirestoreSource {
  final MyConnectivity connectivity;
  final String collectionName;
  FireStoreSourceImpl(this.connectivity, this.collectionName) : super(collectionName, null);

  @override
  Future<WebServiceResponse<List<User>>> getUsers() async {
    List<User> _listUsers = [];
    return getData().then((value) {
      QuerySnapshot result = value.result;
      if (result != null) {
        result.docs.forEach((doc) {
          Map<String, dynamic> document = doc.data();
          document['id'] = doc.id;
          _listUsers.add(User.fromJson(document));
        });
        return WebServiceResponse(result: _listUsers);
      } else {
        throw CustomException(Messages.errorUnpackingData);
      }
    });
  }

  @override
  Future<WebServiceResponse<bool>> addUser(User user) {
    return addEditData(user.toMap(), true);
  }

  @override
  Future<WebServiceResponse<bool>> editUser(User user) {
    return addEditData(user.toMap(), false, dataId: user.id);
  }

  @override
  Future<WebServiceResponse<bool>> deleteUser(String id) {
    return deleteData(id);
  }

  @override
  Stream<List<User>> streamUsers() {
    var data = FirebaseFirestore.instance.collection(collectionName).snapshots().map(
          (event) => event.docs.map((e) {
              Map<String, dynamic> document = e.data();
              document['id'] = e.id;
              return User.fromJson(document);
            },).toList(),
    );
    return data;
  }


}