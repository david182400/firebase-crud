import 'package:connectivity/connectivity.dart';
import 'package:firebase_crud/utils/connectivity/my_connectivity.dart';

class MyConnectivityImpl implements MyConnectivity {
  @override
  Future<ConnectivityResult> checkConnectivity() async {
    return Connectivity().checkConnectivity();
  }
}
