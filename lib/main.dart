import 'dart:async';
import 'dart:developer';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_crud/notifications/notifications_helper.dart';
import 'package:firebase_crud/settings/app_settings.dart';
import 'package:firebase_crud/ui/home_page.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

void main() async {

  WidgetsFlutterBinding.ensureInitialized();
  Firebase.initializeApp().then((value) {

    runZonedGuarded<Future<void>>(() async {
      AppSettings().collectionName = 'users';
      AppSettings().baseUrl = 'https://autosuggest.search.hereapi.com/v1/autosuggest';

      NotificationsHelper().initializedMethods();
      AppSettings().notificationToken = await FirebaseMessaging.instance.getToken();
      print('Token notifications ${AppSettings().notificationToken}');

      FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;

      runApp(MyApp());
    }, FirebaseCrashlytics.instance.recordError);

  }).catchError((error) {
    log(error.toString(), name: 'initializeFlutterFire');
  });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    FirebaseAnalytics analytics = FirebaseAnalytics();
    //FirebaseCrashlytics.instance.crash();
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      home: HomePage(),
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
    );
  }
}
