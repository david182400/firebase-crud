import 'dart:async';

import 'package:firebase_crud/ui/search_page.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart' as location;
import 'package:models/place.dart';
import 'package:permission_handler/permission_handler.dart';

const kGoogleApiKey = "AIzaSyBzS0WDJNXbXXXTdpWB7SHaAj51id-3dm4";

class MapPage extends StatefulWidget {
  const MapPage({Key key}) : super(key: key);

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  CameraPosition _initialPosition = CameraPosition(target: LatLng(7.079816, -73.171520), zoom: 12,);
  Completer<GoogleMapController> _controller = Completer();

  final Set<Marker> _markers = Set();

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  @override
  void initState() {
    super.initState();
    _checkPermission();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(
              icon: Icon(Icons.search),
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => SearchPage(),)).then((value) async {
                  if (value != null) {
                    Place place = value;
                    final GoogleMapController controller = await _controller.future;
                    setState(() {
                      _markers.clear();
                    });
                    controller.animateCamera(CameraUpdate.newCameraPosition(
                        CameraPosition(
                            target: LatLng(place.lat, place.lng),
                            zoom: 16
                        )
                    )).then((value){
                      setState(() {
                        _markers.add(
                          Marker(
                              markerId: MarkerId(place.id),
                              position: LatLng(place.lat, place.lng),
                              infoWindow: InfoWindow(title: place.title, snippet: place.address)
                          ),
                        );
                      });
                    });
                  }
                });
              },)
          ],
        ),
        body: Stack(
          children: [
            Container(
              padding: EdgeInsets.all(10.0),
              child: GoogleMap(
                onMapCreated: _onMapCreated,
                initialCameraPosition: _initialPosition,
                myLocationButtonEnabled: false,
                myLocationEnabled: true,
                markers: _markers,
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.location_on),
          onPressed: currentLocation,
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
      ),
    );
  }

  Future<void> _checkPermission() async {
    final serviceStatus = await Permission.locationWhenInUse.serviceStatus;
    final isGpsOn = serviceStatus == ServiceStatus.enabled;
    if (!isGpsOn) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Please turn on location services')));
      return false;
    }
    final status = await Permission.locationWhenInUse.request();
    if (status == PermissionStatus.denied) {
      _checkPermission();
    } else if (status == PermissionStatus.permanentlyDenied) {
      print('Take the user to the settings page.');
      await openAppSettings();
    }
  }

  currentLocation() async {
    final GoogleMapController controller = await _controller.future;

    location.LocationData currentLocation;
    var localLocation = new location.Location();

    currentLocation = await localLocation.getLocation();

    controller.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: 16
      )
    ));
  }

}