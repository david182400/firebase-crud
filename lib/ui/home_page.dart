import 'package:data/user_repository.dart';
import 'package:domain/user_user_case.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

import 'package:firebase_crud/bloc/user_bloc.dart';
import 'package:firebase_crud/data_source/api_source/places_api_source.dart';
import 'package:firebase_crud/data_source/firestore_source/users_firestore_source.dart';
import 'package:firebase_crud/notifications/notifications_helper.dart';
import 'package:firebase_crud/settings/app_settings.dart';
import 'package:firebase_crud/ui/create_user_page.dart';
import 'package:firebase_crud/ui/map_page.dart';
import 'package:firebase_crud/utils/connectivity/my_connectivity_impl.dart';
import 'package:firebase_crud/utils/utils.dart';
import 'package:firebase_crud/widgets/item_list.dart';
import 'package:firebase_crud/widgets/show_error_screen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:models/custom_exception.dart';
import 'package:models/user.dart';
import 'package:models/web_service_response.dart';


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  UserBloc _userBloc;
  String lastId = '';

  @override
  void initState() {
    super.initState();
    _userBloc = UserBloc(
        UserUseCaseImpl(
            UserRepositoryImpl(
                FireStoreSourceImpl(MyConnectivityImpl(), AppSettings().collectionName),
                PlacesApiSourceAdapter('', MyConnectivityImpl())
            )
        )
    );
    _userBloc.streamUsers();
    FirebaseMessaging.instance.getInitialMessage().then((message) {
      if (message != null) {
        User user = User.fromJson(message.data);
        Navigator.push(context, MaterialPageRoute(builder: (_) => CreateUserPage(isAdd: false, user: user,),));
      }
    });
    NotificationsHelper().streamNotification.listen((message) async {
      message['open_from'] = 'notification';
      await FirebaseAnalytics().logEvent(
        name: 'open_create_user_page',
        parameters: message
      );
      print('data: $message');
      User user = User.fromJson(message);
      if (lastId == '' || lastId != user.id) {
        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (_) => CreateUserPage(isAdd: false, user: user,),), (Route<dynamic> route) {
          return !route.willHandlePopInternally && route.isFirst;
        }).then((value) => lastId = '');
      }
      lastId = user.id;
    });
  }

  @override
  void dispose() {
    _userBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Page'),
        actions: [
          IconButton(icon: Icon(Icons.map), onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (_) => MapPage()));
          })
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: RefreshIndicator(
          child: _ShowList(userBloc: _userBloc,),
          onRefresh: () async {
            _userBloc.streamUsers();
            return Future.value(null);
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          await FirebaseAnalytics().logEvent(
              name: 'open_create_user_page',
              parameters: {'open_from' : 'floating_action_button',}
          );
          Navigator.push(context, MaterialPageRoute(builder: (_) => CreateUserPage(isAdd: true,),));
        },
      ),
    );
  }
}

class _ShowList extends StatelessWidget {

  final UserBloc userBloc;
  const _ShowList({
    @required this.userBloc
  });

  @override
  Widget build(BuildContext context) {
    List<User> listUsers = [];
    return StreamBuilder<WebServiceResponse<List<User>>>(
      stream: userBloc.listMovies,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          WebServiceResponse<List<User>> resp = snapshot.data;
          if (resp.errorMsg != '' || resp.result.isEmpty) {
            return ShowErrorScreen(
              errorMsg: resp.errorMsg ?? 'No existen usuarios',
              onRefresh: () async {
                userBloc.streamUsers();
                return Future.value(null);
              },
            );
          } else {
            listUsers = resp.result;
            return ListView.builder(
              itemCount: listUsers.length,
              itemBuilder: (context, index) {
                return CreateListItem(
                  index: index,
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 8.0),
                    child: ListTile(
                      title: Text(listUsers[index].name, style: TextStyle(fontSize: 18.0),),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(listUsers[index].address),
                          Text(listUsers[index].age.toString() + ' years old')
                        ],
                      ),
                    ),
                  ),
                  onEdit: () {
                    Navigator.push(context, MaterialPageRoute(builder: (_) => CreateUserPage(isAdd: false, user: listUsers[index],),));
                  },
                  onDelete: () {
                    showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          title: Text('Información!'),
                          content: Text('Desea eliminar el registro?'),
                          actions: [
                            TextButton(
                              child: Text('Cancelar'),
                              onPressed: (){
                                Navigator.pop(context);
                              },
                            ),
                            SizedBox(width: 50.0,),
                            TextButton(
                              child: Text('Eliminar'),
                              onPressed: () {
                                userBloc.deleteUser(listUsers[index].id).then((value) {
                                  Navigator.pop(context);
                                  showSnackBar('Información eliminada', context);
                                }).catchError((error) {
                                  Navigator.pop(context);
                                  if (error is CustomException) {
                                    showSnackBar('Error: ${error.msg}', context);
                                  } else {
                                    showSnackBar('Error: $error', context);
                                  }
                                });
                              },
                            ),
                          ],
                        );
                      },
                    );
                  },
                );
              },
            );
          }
        } else {
          return Center(child: CircularProgressIndicator(),);
        }
      },
    );
  }

}
