import 'package:data/user_repository.dart';
import 'package:domain/user_user_case.dart';
import 'package:firebase_crud/bloc/search_places_bloc.dart';
import 'package:firebase_crud/data_source/api_source/places_api_source.dart';
import 'package:firebase_crud/data_source/firestore_source/users_firestore_source.dart';
import 'package:firebase_crud/settings/app_settings.dart';
import 'package:firebase_crud/utils/connectivity/my_connectivity_impl.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:models/place.dart';
import 'package:models/web_service_response.dart';


class SearchPage extends StatefulWidget {
  const SearchPage({Key key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  Place resulPlace;
  SearchPlacesBloc _searchPlacesBloc = SearchPlacesBloc(
    UserUseCaseImpl(
      UserRepositoryImpl(
          FireStoreSourceImpl(MyConnectivityImpl(), AppSettings().collectionName),
          PlacesApiSourceAdapter(AppSettings().baseUrl, MyConnectivityImpl())
      )
    )
  );

  List<Place> placesList = [];

  @override
  void dispose() {
    _searchPlacesBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: WillPopScope(
          onWillPop: () async {
            Navigator.pop(context, resulPlace ?? null);
            return false;
          },
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  child: Row(
                    children: [
                      IconButton(
                        icon: Icon(Icons.arrow_back),
                        onPressed: (){
                          Navigator.pop(context, resulPlace ?? null);
                        },
                      ),
                      Expanded(
                        child: TextField(
                          cursorHeight: 20,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)
                            ),
                            isDense: true
                          ),
                          style: TextStyle(fontSize: 20),
                          onChanged: (value) {
                            if (value.length > 0) {
                              _searchPlacesBloc.getPlacesHttp(value);
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 30,),
                Expanded(
                  child: StreamBuilder(
                    stream: _searchPlacesBloc.listPlaces,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        WebServiceResponse resp = snapshot.data;
                        if (resp.errorMsg != '' || resp.result.isEmpty) {
                          return Container(
                            child: Center(child: Text(resp.errorMsg ?? 'No existen resultados'),),
                          );
                        } else {
                          placesList = resp.result;
                          return ListView.builder(
                            itemCount: placesList.length,
                            itemBuilder: (context, index) {
                              return ListTile(
                                leading: Icon(Icons.location_on_outlined),
                                title: Text(placesList[index].title),
                                subtitle: Text(placesList[index].address),
                                onTap: () {
                                  resulPlace = placesList[index];
                                  Navigator.pop(context, resulPlace ?? null);
                                },
                              );
                            },
                          );
                        }
                      } else {
                        return Center(child: CircularProgressIndicator(),);
                      }
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

