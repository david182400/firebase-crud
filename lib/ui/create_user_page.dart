import 'package:data/user_repository.dart';
import 'package:domain/user_user_case.dart';
import 'package:firebase_crud/bloc/user_bloc.dart';
import 'package:firebase_crud/data_source/api_source/places_api_source.dart';
import 'package:firebase_crud/data_source/firestore_source/users_firestore_source.dart';
import 'package:firebase_crud/settings/app_settings.dart';
import 'package:firebase_crud/utils/connectivity/my_connectivity_impl.dart';
import 'package:firebase_crud/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:models/custom_exception.dart';
import 'package:models/user.dart';


class CreateUserPage extends StatefulWidget {
  final bool isAdd;
  final User user;
  const CreateUserPage({
    @required this.isAdd,
    this.user
  });

  @override
  _CreateUserPageState createState() => _CreateUserPageState();
}

class _CreateUserPageState extends State<CreateUserPage> {
  final TextEditingController _controllerName = new TextEditingController();
  final TextEditingController _controllerAge = new TextEditingController();
  final TextEditingController _controllerAddress = new TextEditingController();
  final TextEditingController _controllerPhone = new TextEditingController();

  bool enableButton = true;

  User user;

  UserBloc _userBloc;

  @override
  void initState() {
    super.initState();
    _userBloc = UserBloc(
        UserUseCaseImpl(
            UserRepositoryImpl(
                FireStoreSourceImpl(MyConnectivityImpl(), AppSettings().collectionName),
                PlacesApiSourceAdapter('', MyConnectivityImpl())
            )
        )
    );
    user = User();
  }

  @override
  void dispose() {
    _controllerName.dispose();
    _controllerAge.dispose();
    _controllerAddress.dispose();
    _controllerPhone.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.user != null) {
      user = widget.user;
      _showMovieData(user);
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.isAdd ? 'Create User' : 'Edit User'),
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              _CreateInput(title: 'Name', textInfo: 'User name', textEditingController: _controllerName, iconData: Icons.person,),
              _CreateInput(title: 'Age', textInfo: 'User age', textEditingController: _controllerAge, iconData: Icons.calendar_today,),
              _CreateInput(title: 'Address', textInfo: 'User address', textEditingController: _controllerAddress, iconData: Icons.house,),
              _CreateInput(title: 'Phone Number', textInfo: 'User phone number', textEditingController: _controllerPhone, iconData: Icons.phone,),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        label: Text(widget.isAdd ? 'Guardar' : 'Editar'),
        onPressed: enableButton ? saveUser : null,
        backgroundColor: enableButton ? Colors.blue : Colors.grey,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  void _showMovieData(User user) {
    _controllerName.text = user.name;
    _controllerAge.text = user.age.toString();
    _controllerAddress.text = user.address;
    _controllerPhone.text = user.phone.toString();
  }

  void saveUser() {
    user.name = _controllerName.text ?? '';
    user.age = int.tryParse(_controllerAge.text);
    user.address = _controllerAddress.text ?? '';
    user.phone = int.tryParse(_controllerPhone.text);
    FocusScope.of(context).requestFocus(new FocusNode());
    setState(() {
      enableButton = false;
    });
    _userBloc.addEditUser(user, widget.isAdd).then((value) {
      String msg = widget.isAdd ? 'Guardada' : 'Modificada';
      showSnackBar('Información $msg', context);
      Navigator.of(context).pop();
    }).catchError((error) {
      if (error is CustomException) {
        showSnackBar('Error: ${error.msg}', context);
      } else {
        showSnackBar('Error: $error', context);
      }
      setState(() {
        enableButton = true;
      });
    });
  }
}


class _CreateInput extends StatelessWidget {
  final TextEditingController textEditingController;
  final String title;
  final String textInfo;
  final IconData iconData;

  const _CreateInput({this.textEditingController, this.title, this.textInfo, this.iconData});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 20.0),
      child: TextField(
        controller: textEditingController,
        decoration: InputDecoration(
          hintText: title,
          labelText: title,
          helperText: textInfo,
          prefixIcon: Icon(iconData),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0)
          ),
        ),
        onEditingComplete: () => FocusScope.of(context).nextFocus(),
        keyboardType: title == 'Age' || title == 'Phone Number' ? TextInputType.numberWithOptions(signed: false, decimal: true) : TextInputType.text,
      ),
    );
  }
}
